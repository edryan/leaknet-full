@echo off
SETLOCAL EnableDelayedExpansion

set VPROJECT=hl2

rem DEBUG
rem del hl2\maps\graphs\d1_trainstation_01.ain

FOR %%I IN (%VPROJECT%\maps\*.bsp) DO (
	rem echo %%~dpI
	rem echo %%~dpIgraphs\%%~nI.ain %%~tI %%~zI
	IF NOT EXIST %%~dpIgraphs\%%~nI.ain (
		set game=%VPROJECT%
		rem echo Pre-changing envvar: !game!
		if "%VPROJECT%"=="hl2" (
			set game=hl3
		)
		rem echo Post-changing envvar: !game!

		rem Skip running the game if VPROJECT is hl2 and the .ain-file has been found in hl3
		set skip=0
		IF EXIST !game!\maps\graphs\%%~nI.ain (
			if not "%VPROJECT%"=="!game!" (
				echo     VPROJECT is hl2, moving the existing .ain from hl3 to hl2...
				if not exist %%~dpIgraphs\ mkdir %%~dpIgraphs\
				move !game!\maps\graphs\%%~nI.ain %%~dpIgraphs\
				IF EXIST %%~dpIgraphs\%%~nI.ain (
					echo         Done!
				)

				set skip=1
			)
		)

		if !skip! equ 0 (
			echo Running %%~nxI of !game!
			hl2.exe -game !game! +map %%~nI +exec nodebuilder +wq

			echo     Checking !game!\maps\graphs\%%~nI.ain for a compiled .ain-file...
			IF EXIST !game!\maps\graphs\%%~nI.ain (
				echo         Done!

				if not "%VPROJECT%"=="!game!" (
					echo     VPROJECT is hl2, moving the compiled .ain from hl3 to hl2...
					echo mmm !game!\maps\graphs\%%~nI.ain m %%~dpIgraphs\ mmm
					if not exist %%~dpIgraphs\ mkdir %%~dpIgraphs\
					move !game!\maps\graphs\%%~nI.ain %%~dpIgraphs\
					IF EXIST %%~dpIgraphs\%%~nI.ain (
						echo         Done!
					)
				)
			)
		)
	)
)

pause
