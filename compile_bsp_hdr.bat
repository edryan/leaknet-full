set mapname=zoo_vguiscreen

set VPROJECT=hl3
bin\vbsp.exe %VPROJECT%\maps\%mapname%.vmf
bin\vvis.exe %VPROJECT%\maps\%mapname%.vmf
bin\vrad.exe %VPROJECT%\maps\%mapname%.vmf
hl2.exe -shader stdshader_hdr_dx9 -game %VPROJECT% -buildcubemaps +map %mapname%
pause