sub CreateFile
{
	local( $filename ) = shift;
	local( *FILE );

	open FILE, ">$filename";
	close FILE;
}

sub DoesTGAMatch
{
	local( $file1 ) = shift;
	local( $file2 ) = shift;
	local( $cmd );
	$cmd = "$texturebuilddir\\bin\\vtf2tga $file1 \\blah1.tga";
	`$cmd`;
	$cmd = "$texturebuilddir\\bin\\vtf2tga $file2 \\blah2.tga";
	`$cmd`;
	$cmd = "$texturebuilddir\\bin\\tgadiff \\blah1.tga \\blah2.tga \\diff.tga"; 
	$tgaoutput = `$cmd`;
	if( $? == 0 )
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

sub TGA2Jpeg
{
	local( $tga ) = shift;
	local( $jpeg ) = shift;
	system "$texturebuilddir\\bin\\cjpeg -targa -quality 100 \"$tga\" \"$jpeg\"";
}

sub GenerateDiffImages
{
	local( $vtf1 ) = shift;
	local( $vtf2 ) = shift;
	local( $outjpg1 ) = shift;
	local( $outjpg2 ) = shift;
	local( $diffjpg ) = shift;

	unlink "\\tmp1.tga";
	unlink "\\tmp2.tga";
	unlink "\\diff.tga";	system "$texturebuilddir\\bin\\vtf2tga $vtf1 \\tmp1.tga";
	system "$texturebuilddir\\bin\\vtf2tga $vtf2 \\tmp2.tga";
	system "$texturebuilddir\\bin\\tgadiff \\tmp1.tga \\tmp2.tga \\diff.tga"; 	
	&TGA2Jpeg( "\\tmp1.tga", $outjpg1 );
	&TGA2Jpeg( "\\tmp2.tga", $outjpg2 );	
	&TGA2Jpeg( "\\diff.tga", $diffjpg );
	unlink "\\tmp1.tga";
	unlink "\\tmp2.tga";
	unlink "\\diff.tga";
}

sub DoesVTFMatch
{
	local( $file1 ) = shift;
	local( $file2 ) = shift;
	local( $cmd ) = "$texturebuilddir\\bin\\vtfdiff $path1 $path2";

        $vtfoutput = `$cmd`;

	if( $? == 0 )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

sub ProcessFile
{
	local( $filename ) = shift;
	local( @fileContents );
	if( $filename =~ /\.vtf/i )
	{
		local( $path1 ) = $filename;
		local( $path2 ) = $filename;
		$path2 =~ s/$texturecomparedir/$texturebuilddir/i;

		$vtfmatch = &DoesVTFMatch( $path1, $path2 );
		$tgamatch = &DoesTGAMatch( $path1, $path2 );
		$filename =~ s/$texturecomparedir//i;
		if( !$vtfmatch )
		{
			print "vtf mismatch $filename\n";
#			print "VTFOUTPUT: $vtfoutput\n";
			local( $jpeg1, $jpeg2, $diffjpeg );
			$jpeg1 = $jpeg2 = $diffjpeg = $path2;
			$jpeg1 =~ s/\.vtf/\_1.jpg/gi;
			$jpeg2 =~ s/\.vtf/\_2.jpg/gi;
			$diffjpeg =~ s/\.vtf/\_diff.jpg/gi;
			&GenerateDiffImages( $path1, $path2, $jpeg1, $jpeg2, $diffjpeg );
		}
		if( !$tgamatch && $vtfmatch )
		{
			print "RUH ROH!!!!!!!!!!!!!!!!! $filename: tgamatch $tgamatch vtfmatch $vtfmatch\n";
			print "TGAOUTPUT: $tgaoutput\n";
			print "VTFOUTPUT: $vtfoutput\n";
			print "----------\n";
		}

	}
}

sub ProcessFileOrDirectory
{
	local( $name ) = shift;

#	If the file has "." at the end, skip it.
	if( $name eq "." || $name eq ".." || $name =~ /\.$/ )
	{
#		print "skipping: $name\n";
		return;
	}

#   Figure out if it's a file or a directory.
	if( -d $name )
	{
		local( *SRCDIR );
#		print "$name is a directory\n";
		opendir SRCDIR, $name;
		local( @dir ) = readdir SRCDIR;
		closedir SRCDIR;

		local( $item );
		while( $item = shift @dir )
		{
			&ProcessFileOrDirectory( $name . "/" . $item );
		}
	}
	elsif( -f $name )
	{
		&ProcessFile( $name );
	}
	else
	{
		print "$name is neither a file or a directory\n";
	}
	return;
}

$texturebuilddir		= shift;
$texturecomparedir 		= shift;
$texturebuilddir =~ s,\\,/,g;
$texturecomparedir =~ s,\\,/,g;

if( !$texturebuilddir || !$texturecomparedir )
{
	die "Usage: buildalltextures.pl texturebuilddir texturecomparedir";
}

print "\$texturecomparedir = \"$texturecomparedir\"\n";
print "\$texturebuilddir = \"$texturebuilddir\"\n";

opendir SRCDIR, $texturecomparedir;
@dir = readdir SRCDIR;
closedir SRCDIR;

while( $item = shift @dir )
{
	&ProcessFileOrDirectory( "$texturecomparedir/$item" );
}


