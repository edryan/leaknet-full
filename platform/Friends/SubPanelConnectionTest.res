"Friends/SubPanelConnectionTest.res"
{
	"RetryButton"
	{
		"ControlName"		"Button"
		"fieldName"		"RetryButton"
		"xpos"		"86"
		"ypos"		"214"
		"wide"		"80"
		"tall"		"24"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"0"
		"enabled"		"1"
		"tabPosition"		"2"
		"labelText"		"&Retry"
		"textAlignment"		"center"
		"dulltext"		"0"
		"brighttext"		"0"
		"Default"		"0"
	}
	"InfoText"
	{
		"ControlName"		"Label"
		"fieldName"		"InfoText"
		"xpos"		"102"
		"ypos"		"26"
		"wide"		"300"
		"tall"		"24"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"		"0"
		"labelText"		"Attempting to connect to tracker servers... failure."
		"textAlignment"		"west"
		"dulltext"		"0"
		"brighttext"		"0"
	}
	"DescriptionText"
	{
		"ControlName"		"Label"
		"fieldName"		"DescriptionText"
		"xpos"		"30"
		"ypos"		"53"
		"wide"		"400"
		"tall"		"200"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"		"0"
		"labelText"		"Tracker could not connect to server.

If you are behind a firewall or proxy, tracker needs to
use the following ports:
    incoming UDP ports 27000, 27001
    outgoing UDP ports 1200, 27000, 27001

Please send email to trackerbugs@valvesoftware.com
if yo"
		"textAlignment"		"center"
		"dulltext"		"0"
		"brighttext"		"0"
	}
}
