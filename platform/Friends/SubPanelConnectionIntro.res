"Friends/SubPanelConnectionIntro.res"
{
	"ProgressBar"
	{
		"ControlName"		"ProgressBar"
		"fieldName"		"ProgressBar"
		"xpos"		"77"
		"ypos"		"122"
		"wide"		"64"
		"tall"		"24"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"		"0"
	}
	"InfoText"
	{
		"ControlName"		"Label"
		"fieldName"		"InfoText"
		"xpos"		"17"
		"ypos"		"73"
		"wide"		"64"
		"tall"		"24"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"		"0"
		"labelText"		"Label"
		"textAlignment"		"west"
		"dulltext"		"0"
		"brighttext"		"0"
	}
	"ProgressText"
	{
		"ControlName"		"Label"
		"fieldName"		"ProgressText"
		"xpos"		"145"
		"ypos"		"175"
		"wide"		"64"
		"tall"		"24"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"		"0"
		"labelText"		"Label"
		"textAlignment"		"west"
		"dulltext"		"0"
		"brighttext"		"0"
	}
}
